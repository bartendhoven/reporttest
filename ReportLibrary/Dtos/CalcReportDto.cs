﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Report.Data.Dtos
{
    public class CalcReportDTO
    {
        public int GROUPORDER { get; set; }
        public int VALUEORDER { get; set; }

        public string SHORTDESCRIPTION { get; set; }

        public string UNIT { get; set; }
        public double VALUE { get; set; }

        public string FORMAT { get; set; }
        public string NAME { get; set; }
        public string BACK_COLOR { get; set; }


    }
}
